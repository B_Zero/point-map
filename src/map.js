import * as L from 'leaflet';

export class Map{
  attached(){
    var map = L.map('mapID').setView([58.364954, 26.741233], 15)
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox.streets',
      accessToken: 'pk.eyJ1IjoiYnplcm8iLCJhIjoiY2pnOWN5cm0wOHNyeTMzbWQ5cG9zd2tpNiJ9.eej4QOIV23cPro8pGZJVYw'
  }).addTo(map);
  }
}
