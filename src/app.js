export class App {
  constructor() {
    this.message = 'Map';
  }

  configureRouter(config, router) {
    this.router = router;
    config.title = 'Map';
    config.map([
      { route: '', moduleId: PLATFORM.moduleName('map')}
    ]);

  }
}
